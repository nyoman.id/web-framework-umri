<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MahasiswaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('mahasiswa.daftar');
});

Route::get('/daftarmahasiswa', [MahasiswaController::class, 'daftar'])->name('mahasiswa.daftar');
Route::get('/tambahmahasiswa', [MahasiswaController::class, 'tambah'])->name('mahasiswa.daftar');
Route::post('/createmahasiswa', [MahasiswaController::class, 'create'])->name('mahasiswa.update');
Route::get('/mahasiswa/{id}/lihat', [MahasiswaController::class, 'lihat'])->name('mahasiswa.lihat');
Route::get('/mahasiswa/{id}/edit', [MahasiswaController::class, 'edit'])->name('mahasiswa.edit');
Route::post('/updatemahasiswa', [MahasiswaController::class, 'update'])->name('mahasiswa.update');
Route::delete('/deletemahasiswa', [MahasiswaController::class, 'delete'])->name('mahasiswa.delete');
