<?php
// app/Http/Controllers/MahasiswaController.php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\DaftarKontak;
use App\Models\Mahasiswa;

class MahasiswaController extends Controller
{
    public function daftar()
    {

        // Mengambil data dari basis data menggunakan model
        $semuaKontak = DaftarKontak::get();
        

        $data_mahasiswa = $semuaKontak;
        return view('mahasiswa', compact('data_mahasiswa'));
    }

    public function lihat($id)
    {

        // Mengambil data dari basis data menggunakan model
        $mahasiswa = DaftarKontak::where('id',$id)->first();

        return view('mahasiswa-detail', compact('mahasiswa'));
    }

    public function tambah()
    {
        // Membuat objek model "DaftarKontak" dan menyimpan data
        $kontak = new DaftarKontak();
        $kontak->nama = "Andi";
        $kontak->telp = "+628111223444";
        $kontak->alamat = "Jalan Raya no 1";
        $kontak->email = "andi123@example.com";
        $kontak->save();

        // Mendapatkan ID save yang terakhir.
        $last_id = $kontak->id;
        $kontakBaru = DaftarKontak::where('id', $last_id)->first();;
        return redirect()->route('mahasiswa.lihat', ['id' => $last_id]);
    }

    public function edit($id) {
        $query_update = [
            "nama" => "Indra Darmawan",
            "telp" => "+62889919"
        ];
        $kontakUpdate = DaftarKontak::where("id",$id)->update($query_update);
        return redirect()->route('mahasiswa.lihat', ['id' => $id]);
        
    }
    public function delete($id) {
        $kontakDelete = DaftarKontak::where("id", $id)->delete();
        return redirect()->route('mahasiswa.daftar');
    }
}
