<?php
// app/Models/DaftarKontak.php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DaftarKontak extends Model
{
    use HasFactory;

    protected $table = 'daftar_kontak';

    // dicomment, krn sudah ada kolom created_at & updated_at
    // public $timestamps = false;
    
    protected $fillable = ['nama','telp','alamat', 'email'];

    public function opt_query() {
        return;
    }

}
