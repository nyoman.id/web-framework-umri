<!-- resources/views/mahasiswa.blade.php -->
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{config('app.name')}}</title>
        <style>
            .odd-row {
                background-color:#d1d1d1;
                color:black;
            }
            .even-row {
                background-color:white;
                color:black
            }
        </style>
    </head>
    <body>
        <h1>Welcome {{config('app.name')}}</h1>
        <table border=1 cellpadding=5 cellspacing=5 style='border-collapse:collapse;'>
        <tr><th>no</th><th>Nama</th><th>Telepon</th><th>Alamat</th><th>Email</th><th>Action</th></tr>
            @foreach($data_mahasiswa as $key => $mhs)
                <tr class="{{ (($key+1) % 2 == 0 ? 'odd-row' : 'even-row') }}">
                <td>{{ $key+1 }}</td>
                <td>{{ $mhs->nama }}</td>
                <td>{{ $mhs->telp}}</td>
                <td>{!! str_replace('Jalan','Jln', $mhs->alamat)  !!}</td>
                <td>{{ $mhs->email}}</td>
                <td><a href="{{ url('mahasiswa/'.$mhs->id.'/lihat') }}">lihat</a></td>
                </tr>
            @endforeach
        </table>
    </body>
</html>