<!-- resources/views/mahasiswa.blade.php -->
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{config('app.name')}}</title>
        <style>
            .odd-row {
                background-color:#d1d1d1;
                color:black;
            }
            .even-row {
                background-color:white;
                color:black
            }
        </style>
    </head>
    <body>
        
        <h3>Hallo, {{ $mahasiswa->nama }}</h3>
        <p>Informasi pribadi anda adalah: </p>
        <ul>
            <li>Alamat: <b>{{ $mahasiswa->alamat }}</b></li>
            <li>Telepon: <b>{{ $mahasiswa->telp }}</b></li>
            <li>Email: <b>{{ $mahasiswa->email }}</b></li>
        </ul>
        <a href="{{ url('daftarmahasiswa') }}">Kembali</i>
    </body>
</html>