<?php
// database/seeders/DaftarKontakTableSeeder.php
namespace Database\Seeders;

use App\Models\DaftarKontak;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DaftarKontakTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $daftar_kontak = [
            ['nama' => 'Nyoman', 'telp' => '+628145679', 'alamat' => 'Jalan 123', 'email' => 'nyoman@example.com'],
            ['nama' => 'Indra', 'telp' => '+628145600', 'alamat' => 'Jalan 345', 'email' => 'indra@example.com'],
            ['nama' => 'Darmawan', 'telp' => '+628145699', 'alamat' => 'Jalan 345', 'email' => 'darmawan@example.com']
        ];
        foreach($daftar_kontak as $kontak){
            DaftarKontak::create($kontak);
        }
    }
}
