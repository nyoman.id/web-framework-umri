<?php
// database/migrations/2023_10_25_042858_create_daftar_kontak_table.php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daftar_kontak', function (Blueprint $table) {
            $table->id();
            $table->string('nama', 75);
            $table->string('telp', 15);
            $table->text('alamat');
            $table->string('email',100)->unique();
            $table->timestamps();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daftar_kontak');
    }
};
